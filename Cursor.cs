﻿// Decompiled with JetBrains decompiler

// Assembly: HolyFortesqueCursor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5F3CF521-8242-4777-9367-03DCBE062C7C


using ShooterCursor.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ShooterCursor;
using ShooterCursor.Helper;

namespace ShooterCursor
{
    public class Cursor : Form
    {
        Gamma.Brightness brightness = new Gamma.Brightness();
        int _defaultBrightness = 0;

        private KeyboardHook hook = new KeyboardHook();
        private KeyboardHook hookMoveUp = new KeyboardHook();
        private KeyboardHook hookMoveDown = new KeyboardHook();
        private KeyboardHook hookMoveLeft = new KeyboardHook();
        private KeyboardHook hookMoveRight = new KeyboardHook();
        private KeyboardHook hookChangeCursor = new KeyboardHook();
        private KeyboardHook hookTopMost = new KeyboardHook();
        private KeyboardHook hookCursorColor = new KeyboardHook();

        private KeyboardHook hookCursorHelp = new KeyboardHook();
        private KeyboardHook hookChangeGamma = new KeyboardHook();

        CursorHelper cursorHelper = new CursorHelper(PathSettings.IndexSizeCursor(), PathSettings.IndexColorCursor());

        private int GammaIndex = 1;
        public Cursor()
        {
            MessageBox.Show("Для получения справки о горячих клавишах, \nв программе (курсор для Шутеров) - нажмите: Contrl + NumPad7 ");
            this.Activate();
            this.TopMost = true;
            this.TransparencyKey = SystemColors.Control;
            this.InitializeComponent();
            this.AllowTransparency = true;
            this.BackColor = Color.Beige;
            this.TransparencyKey = this.BackColor;

            this.hook.KeyPressed += new EventHandler<KeyPressedEventArgs>(this.CursorMoveUp);
            this.hook.RegisterHotKey(ShooterCursor.ModifierKeys.Control, Keys.NumPad8);

            this.hookMoveDown.KeyPressed += new EventHandler<KeyPressedEventArgs>(this.CursorMoveDown);
            this.hookMoveDown.RegisterHotKey(ShooterCursor.ModifierKeys.Control, Keys.NumPad2);

            this.hookMoveLeft.KeyPressed += new EventHandler<KeyPressedEventArgs>(this.CursorMoveLeft);
            this.hookMoveLeft.RegisterHotKey(ShooterCursor.ModifierKeys.Control, Keys.NumPad4);

            this.hookMoveRight.KeyPressed += new EventHandler<KeyPressedEventArgs>(this.CursorMoveRight);
            this.hookMoveRight.RegisterHotKey(ShooterCursor.ModifierKeys.Control, Keys.NumPad6);

            this.hookChangeCursor.KeyPressed += new EventHandler<KeyPressedEventArgs>(this.SizeCursor);
            this.hookChangeCursor.RegisterHotKey(ShooterCursor.ModifierKeys.Control, Keys.NumPad9);

            this.hookCursorHelp.KeyPressed += new EventHandler<KeyPressedEventArgs>(this.CursorHelp);
            this.hookCursorHelp.RegisterHotKey(ShooterCursor.ModifierKeys.Control, Keys.NumPad7);

            this.hookCursorColor.KeyPressed += new EventHandler<KeyPressedEventArgs>(this.CursorColor);
            this.hookCursorColor.RegisterHotKey(ShooterCursor.ModifierKeys.Control, Keys.NumPad1);

            this.hookChangeGamma.KeyPressed += new EventHandler<KeyPressedEventArgs>(this.GammaChange);
            this.hookChangeGamma.RegisterHotKey(ShooterCursor.ModifierKeys.Control, Keys.NumPad5);

            if (PathSettings.coordinateX() != 0 && PathSettings.coordinateY() != 0)
            {
                this.Location = new Point(PathSettings.coordinateX(), PathSettings.coordinateY());
            }

            //if (PathSettings.IndexColorCursor() != cursorHelper.indexColorCursor)
            //{
            cursorHelper.ChangeColorCursor(PathSettings.IndexColorCursor());
            //}

            //if (PathSettings.IndexSizeCursor() != cursorHelper.indexSizeCursor)
            //{
            ClientSize = cursorHelper.ChangeSizeCursor(PathSettings.IndexSizeCursor());
            //}
        }

        private void SizeCursor(object sender, EventArgs e)
        {
            ClientSize = cursorHelper.ChangeSizeCursor(cursorHelper.NextSizeCursor());
        }

        private void CursorColor(object sender, EventArgs e)
        {
            cursorHelper.ChangeColorCursor(cursorHelper.NextColorCursor());
        }

        private void CursorHelp(object sender, EventArgs e)
        {
            string message = null;

            message = "Contrl + NumPad7 - просмотр горячих клавиш \n" +
                    "Contrl + NumPad9 - Размер прицела \n" +
                    "Contrl + NumPad8 - Перемещать прицел вверх \n" +
                    "Contrl + NumPad2 - Перемещать прицел вниз \n" +
                    "Contrl + NumPad4 - Перемещать прицел влево \n" +
                    "Contrl + NumPad6 - Перемещать прицел вправо \n" +
                    "Contrl + NumPad1 - Смена цвета прицела \n" +
                    "Contrl + NumPad5 - Смена яркости. Поддерживается не для всех типов мониторов или видеокарт =( \n";
            MessageBox.Show(message);
        }

        private void CursorMoveRight(object sender, EventArgs e)
        {
            this.Location = new Point((this.Location.X + 1), this.Location.Y);
        }

        private void CursorMoveLeft(object sender, EventArgs e)
        {
            this.Location = new Point(this.Location.X - 1, this.Location.Y);
        }

        private void CursorMoveUp(object sender, EventArgs e)
        {
            this.Location = new Point(this.Location.X, this.Location.Y - 1);
        }

        private void CursorMoveDown(object sender, EventArgs e)
        {
            this.Location = new Point(this.Location.X, this.Location.Y + 1);
        }


        private void GammaChange(object sender, EventArgs e)
        {
            DefaultBrightness(_defaultBrightness);
            if (GammaIndex == 1)
            {
                GammaMax();
                GammaIndex = 2;
            }
            else
            {
                GammaNormal(_defaultBrightness);
                GammaIndex = 1;
            }

        }

        private int DefaultBrightness(int defaultBrightness)
        {
            if (defaultBrightness == 0)
            {
                _defaultBrightness = brightness.Value;
            }

            return _defaultBrightness;
        }

        private void GammaMax()
        {
            brightness.Value = 256;
        }

        private void GammaNormal(int gamma)
        {
            brightness.Value = gamma;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && cursorHelper.components != null)
                cursorHelper.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            cursorHelper.pictureBoxCrosshair = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(cursorHelper.pictureBoxCrosshair)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCrosshair
            // 
            cursorHelper.pictureBoxCrosshair.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            cursorHelper.pictureBoxCrosshair.BackColor = System.Drawing.Color.Transparent;
            cursorHelper.pictureBoxCrosshair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            cursorHelper.pictureBoxCrosshair.Dock = System.Windows.Forms.DockStyle.Fill;
            cursorHelper.pictureBoxCrosshair.Enabled = false;
            cursorHelper.pictureBoxCrosshair.ErrorImage = global::ShooterCursor.Properties.Resources.DotRed;
            cursorHelper.pictureBoxCrosshair.Image = global::ShooterCursor.Properties.Resources.DotRed;
            cursorHelper.pictureBoxCrosshair.Location = new System.Drawing.Point(0, 0);
            cursorHelper.pictureBoxCrosshair.Name = "pictureBoxCrosshair";
            cursorHelper.pictureBoxCrosshair.Size = new System.Drawing.Size(3, 3);
            cursorHelper.pictureBoxCrosshair.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            cursorHelper.pictureBoxCrosshair.TabIndex = 0;
            cursorHelper.pictureBoxCrosshair.TabStop = false;
            // 
            // Cursor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(3, 3);
            this.ControlBox = false;
            this.Controls.Add(cursorHelper.pictureBoxCrosshair);
            this.Enabled = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimizeBox = false;
            this.Name = "Cursor";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Cursor_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(cursorHelper.pictureBoxCrosshair)).EndInit();
            this.ResumeLayout(false);

        }

        private void Cursor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cursorHelper.NeedUpdateCoordinateCursor(Location.X, Location.Y) || cursorHelper.NeedUpdateColorCursor() || cursorHelper.NeedUpdateSizeCursor())
                cursorHelper.SaveCursorSettings(Location.X, Location.Y);
        }
    }
}

