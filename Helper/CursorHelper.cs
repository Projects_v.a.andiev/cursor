﻿using ShooterCursor.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShooterCursor.Helper
{
    public class CursorHelper
    {

        public CursorHelper(int sizeCursor, int colorCursor)
        {
            indexSizeCursor = sizeCursor;
            indexColorCursor = colorCursor;
        }

        public int indexSizeCursor { get; set; }
        public int indexColorCursor { get; set; }

        public IContainer components;
        public PictureBox pictureBoxCrosshair;
        public Image colorCursor = (Image)Resources.DotRed;


        public Size ChangeSizeCursor(int sizeCursor)
        {
            pictureBoxCrosshair.Size = new Size(indexSizeCursor, indexSizeCursor);
            return new Size(indexSizeCursor, indexSizeCursor);
        }

        public int NextSizeCursor()
        {
            if (indexSizeCursor < 5)
            {
                indexSizeCursor++;
            }
            else
            {
                indexSizeCursor = 2;
            }

            return indexSizeCursor;
        }

        public int NextColorCursor()
        {
            if (indexColorCursor < 4)
            {
                indexColorCursor++;
            }
            else
            {
                indexColorCursor = 1;
            }

            return indexColorCursor;
        }

        public void ChangeColorCursor(int colorIndex)
        {
            switch (colorIndex)
            {
                case 1:
                    colorCursor = (Image)Resources.DotRed;
                    pictureBoxCrosshair.Image = colorCursor;
                    //this.CursorColorIndex = 2;
                    break;
                case 2:
                    colorCursor = (Image)Resources.DotGreen;
                    pictureBoxCrosshair.Image = colorCursor;
                    //this.CursorColorIndex = 3;
                    break;
                case 3:
                    colorCursor = (Image)Resources.DotBlue;
                    pictureBoxCrosshair.Image = colorCursor;
                    //this.CursorColorIndex = 4;
                    break;
                case 4:
                    colorCursor = (Image)Resources.DotWhite;
                    pictureBoxCrosshair.Image = colorCursor;
                    //this.CursorColorIndex = 1;
                    break;
            }
        }

        public void SaveCursorSettings(int currentPositionX, int currentPositionY)
        {
            switch (MessageBox.Show("Сохранить изменения настроек курсора?",
              "Настроки расположения курсора",
              MessageBoxButtons.YesNo,
              MessageBoxIcon.Question))
            {
                case DialogResult.Yes:
                    SaveCoordinateCursor(currentPositionX, currentPositionY);
                    SaveColorCursor();
                    SaveSizeCursor();
                    MessageBox.Show("Настройка сохранена");
                    break;

                case DialogResult.No:
                    //MessageBox.Show("Отмена сохранения");
                    break;
                default:
                    //MessageBox.Show("Отмена сохранения");
                    break;
            }
        }

        public void SaveCoordinateCursor(int currentPositionX, int currentPositionY)
        {
            PathSettings.UpdateSetting("X", currentPositionX.ToString());
            PathSettings.UpdateSetting("Y", currentPositionY.ToString());
        }

        public void SaveColorCursor()
        {
            PathSettings.UpdateSetting("IndexColorCursor", indexColorCursor.ToString());
        }

        public void SaveSizeCursor()
        {
            PathSettings.UpdateSetting("IndexSizeCursor", indexSizeCursor.ToString());
        }

        public bool NeedUpdateCoordinateCursor(int currentPositionX, int currentPositionY)
        {
            if (PathSettings.coordinateX() != currentPositionX || PathSettings.coordinateY() != currentPositionY)
            {
                return true;
            }

            return false;
        }

        public bool NeedUpdateColorCursor()
        {
            if (PathSettings.IndexColorCursor() != indexColorCursor)
            {
                return true;
            }

            return false;
        }

        public bool NeedUpdateSizeCursor()
        {
            if (PathSettings.IndexSizeCursor() != indexSizeCursor)
            {
                return true;
            }

            return false;
        }
    }
}
