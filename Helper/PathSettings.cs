﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShooterCursor
{
   public static class PathSettings
    {
        public static int coordinateX()
        {
            return int.Parse(ConfigurationManager.AppSettings["X"]);
        }

        public static int coordinateY()
        {
            return int.Parse(ConfigurationManager.AppSettings["Y"]);
        }

        public static int IndexColorCursor()
        {
            return int.Parse(ConfigurationManager.AppSettings["IndexColorCursor"]);
        }

        public static int IndexSizeCursor()
        {
            return int.Parse(ConfigurationManager.AppSettings["IndexSizeCursor"]);
        }

        


        public static void UpdateSetting(string key, string value)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var entry = config.AppSettings.Settings[key];
            if (entry == null)
                config.AppSettings.Settings.Add(key, value);
            else
                config.AppSettings.Settings[key].Value = value;

            config.Save(ConfigurationSaveMode.Modified);
        }
    }
}
