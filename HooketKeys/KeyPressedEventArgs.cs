﻿// Decompiled with JetBrains decompiler
// Type: ShooterCursor.KeyPressedEventArgs
// Assembly: HolyFortesqueCursor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5F3CF521-8242-4777-9367-03DCBE062C7C
// Assembly location: D:\prj\fun\HolyFortesqueCursor good.exe

using System;
using System.Windows.Forms;

namespace ShooterCursor
{
  public class KeyPressedEventArgs : EventArgs
  {
    private ModifierKeys _modifier;
    private Keys _key;

    public ModifierKeys Modifier
    {
      get
      {
        return this._modifier;
      }
    }

    public Keys Key
    {
      get
      {
        return this._key;
      }
    }

    internal KeyPressedEventArgs(ModifierKeys modifier, Keys key)
    {
      this._modifier = modifier;
      this._key = key;
    }
  }
}
