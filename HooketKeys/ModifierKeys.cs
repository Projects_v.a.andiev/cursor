﻿// Decompiled with JetBrains decompiler
// Type: ShooterCursor.ModifierKeys
// Assembly: HolyFortesqueCursor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5F3CF521-8242-4777-9367-03DCBE062C7C
// Assembly location: D:\prj\fun\HolyFortesqueCursor good.exe

using System;

namespace ShooterCursor
{
  [Flags]
  public enum ModifierKeys : uint
  {
    Alt = 1U,
    Control = 2U,
    Shift = 4U,
    Win = 8U,
  }
}
