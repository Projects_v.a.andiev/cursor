﻿// Decompiled with JetBrains decompiler
// Type: ShooterCursor.KeyboardHook
// Assembly: HolyFortesqueCursor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5F3CF521-8242-4777-9367-03DCBE062C7C
// Assembly location: D:\prj\fun\HolyFortesqueCursor good.exe

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ShooterCursor
{
  public sealed class KeyboardHook : IDisposable
  {
    private KeyboardHook.Window _window = new KeyboardHook.Window();
    private int _currentId;

    public event EventHandler<KeyPressedEventArgs> KeyPressed;

    public KeyboardHook()
    {
      this._window.KeyPressed += (EventHandler<KeyPressedEventArgs>) ((sender, args) =>
      {
        if (this.KeyPressed == null)
          return;
        this.KeyPressed((object) this, args);
      });
    }

    [DllImport("user32.dll")]
    private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

    [DllImport("user32.dll")]
    private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

    public void RegisterHotKey(ModifierKeys modifier, Keys key)
    {
      this._currentId = this._currentId + 1;
      if (!KeyboardHook.RegisterHotKey(this._window.Handle, this._currentId, (uint) modifier, (uint) key))
        throw new InvalidOperationException("Couldn’t register the hot key.");
    }

    public void Dispose()
    {
      for (int id = this._currentId; id > 0; --id)
        KeyboardHook.UnregisterHotKey(this._window.Handle, id);
      this._window.Dispose();
    }

    private class Window : NativeWindow, IDisposable
    {
      private static int WM_HOTKEY = 786;

      public event EventHandler<KeyPressedEventArgs> KeyPressed;

      public Window()
      {
        this.CreateHandle(new CreateParams());
      }

      protected override void WndProc(ref Message m)
      {
        base.WndProc(ref m);
        if (m.Msg != KeyboardHook.Window.WM_HOTKEY)
          return;
        Keys key = (Keys) ((int) m.LParam >> 16 & (int) ushort.MaxValue);
        ModifierKeys modifier = (ModifierKeys) ((int) m.LParam & (int) ushort.MaxValue);
        if (this.KeyPressed == null)
          return;
        this.KeyPressed((object) this, new KeyPressedEventArgs(modifier, key));
      }

      public void Dispose()
      {
        this.DestroyHandle();
      }
    }

    internal void RegisterHotKey(Keys keys1, Keys keys2)
    {
        throw new NotImplementedException();
    }
  }
}
