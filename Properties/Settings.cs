﻿// Decompiled with JetBrains decompiler
// Type: ShooterCursor.Properties.Settings
// Assembly: HolyFortesqueCursor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5F3CF521-8242-4777-9367-03DCBE062C7C
// Assembly location: D:\prj\fun\HolyFortesqueCursor good.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace ShooterCursor.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }
  }
}
